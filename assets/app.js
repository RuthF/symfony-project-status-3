/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import './scss/animate.scss';

import './js/jquery.js';

import './scss/icomoon.scss';
import './scss/bootstrap.scss';
import './scss/magnific-popup.scss';
import './scss/flexslider.scss';
import './scss/owl.carousel.scss';
import './scss/owl.theme.default.scss';
import './scss/style.scss';


import './js/modernizr-2.6.2.js';
import './js/jquery.easing.1.3.js';
import './js/jquery.waypoints.js';
import './js/jquery.flexslider.js';
import './js/owl.carousel.js';
import './js/jquery.magnific-popup.js';
import './js/magnific-popup-options.js';
import './js/jquery.countTo.js';
import './js/main.js';
